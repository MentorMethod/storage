echo "Making tarball"
echo ""
tar -cv /tank/path/to/directory -f /tank/path/to/tarball/tarball.tar
echo ""
echo "LZOP Compressing"
lzop -9 -v -p /tank/path/to/tarball/tarball.tar 
echo "" 
echo "Testing archive" 
lzop -tv /tank/path/to/compressed/tarball.tar.lzo
echo ""
echo "Encrypting archive"
rage --encrypt -r [YOUR AGE PUBLIC KEY] -o /tank/path/to/encrypted/tarball.tar.lzo.age /tank/path/to/compressed/tarball/tarball.tar.lzo 
echo "Empy output successful encryption"
