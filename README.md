# storage

Distributed, redundant, encrypted storage across commodity cloud providers. 

The name is oddly appropriate: stor is swedish for great, large and high (cloud anyone), {r}age is the encryption software.

## Getting started

The purpose of this project is to use commodity (and local) storage providers to keep backups, archives, and other storage but to remove the need for trusting their capabilities, integrity, or availability. This means using multiple providers, generating a local archive and verifying it, and encrypting with modern encryption for ensured confidentiality. 

Previously, I used par2 encoding at 50% and I'd recommend that for anyone using less than 5 providers. I encourage you to use storage providers that are geographically diverse. Think of this like RAID but Redundant Array of Independent Storage. Each provider provides more redundancy but if you value your data or are afraid of corruption, par2 encoding can fix that corruption. It's been long trusted and tested in the world of Torrents and Usenet. 

As a point of clarification, I do encourage you to pay for your storage as being a leech is bad, but you also can assume to have justificiation to request better service and support those who supporting you. However, sometimes confidence in the provider is low, the bandwidth is slow, and you just want to add another provider. By all means go ahead and do that, but understand that provider has no responsibility to you, your account, and if they're that bad I wouldn't make them one of your top locations. 

Due to the need for setting the configuration file password as an environment
variable I'd recommend storing these files on an **external media** so if your system is compromised by an attacker or if you experience a system failure you can recover.

## Setup Prerequisites

1. [rclone](https://rclone.org/downloads/) from the source
  - `curl https://rclone.org/install.sh | sudo bash`
  - Yes, packages are better, but they're often behind the latest version and latest available storage providers. 
2. Rust, Cargo, [Rage](https://github.com/str4d/rage)
  - Install Rust and Cargo for your distribution then `cargo rage`
  - Put your cargo PATH into your user's profile
  - `source ~user/.profile` if you're not relaunching your shell
3. Choose your [storage providers](https://rclone.org/overview/) and sign up for an account
  - Personally, I've paid for and had great success with use pCloud, Tardigrade/Storj, Backblaze B2 and Google Drive. 
  - There are many free tiers for accounts, just be aware those may disappear and you're not a "customer"
  
## Storage Provider Walkthrough

1. Satisfy prerequisites. (see above)
2. run 'rclone config' and set a configuration password (Option 's') 
3. Choose setup a 'n'ew remote  (Option 'n') 
4. Using Backblaze B2 as an example: 
   - Sign up for an account
   - Verify email and phone number
   - Setup an Application Key (avoid using your primary account!) 
5. Select Backblaze B2 from the list of "remotes"
6. Give a descriptive name e.g. "blaze" 
7. Copy the the Application ID and Application Key 
8. Accept defaults in most cases, your choice. 
9. Make a 'n'ew remote "crypt" (12)
10. Again, give it a descriptive name e.g. "blazecrypt" 
11. Label your remote as you did previously: blaze:your-bucket-name
12. Accept defaults: encrypt filenames, encrypt directories
13. Generate random password and salt 
    - You don't need to save these, they're in your encrypted config file

Rinse and repeat for each storage provider you want to use.

**RETAIN AND KEEP AT LEAST ONE LOCAL UNENCRYPTED STORAGE LOCATION UNTIL YOU COMPLETE ALL STEPS SUCCESSFULLY AND ARE COMFORTABLE WITH THE PROCESS**

## Prep.sh Setup

Eventually I'll make this a bit more elegant, but this is how it works: 

1. Make a tarball of a directory
2. LZOP -9 compress the tarball
   - Why LZOP? It's reproducible decreasing sync time for multiple runs
   - It has checksum values embedded vs using par2 
   - It is middle of the road in speed, compression, and CPU I/O
     ![Compression Compared](mksa12-proxmox-auszug.png)
3. Test that the compression didn't corrupt anything using lzop checksums
4. Encrypt it to {r}age public key
   - Generate your rage key with `rage-keygen`
   - Offline your private key when not testing your recovery

## Rclone.sh Setup

1. `sh ./prep.sh` 
   - Runs the tarball, compression, and encryption file you just setup. 
2. echo "Storage Provider N/N"
   - Storage provider name for status updates
3. RCLONE_CONFIG_PASS is the pass you setup at the beginning
   - No, this isn't the best approach but this isn't intended to be left online
4. `rclone --verbose sync /path/to/archive.tar.lzo.age storage:bucket`
5. `rclone ls storage:bucket` 
    Validation to ensure completion e.g. upload is the same across providers. 

### Future Plans

- arg variables
- basic settings
- make functions and loops
- [port to Rust](https://github.com/rust-shell-script/rust_cmd_lib)
- library-ize 
