NOW=$(date --utc "+%FT%T.%3NZ")
LOGFILE="log-$NOW.log"

echo "Edit prep.sh with your file and encryption keys"

echo "Uncomment prep.sh once completed"

# sh ./prep.sh

echo "Update this section with your config password, the path to your encrypted archive, storage provider names and your bucket with them." 

## echo "Backblaze B2 1/2"
## RCLONE_CONFIG_PASS="[YOUR CONFIG PASSWORD]" rclone --verbose --log-file=/tank/user/logs/$LOGFILE --log-level=DEBUG sync /path/to/archive.tar.lzo.age blzecrypt:bucket-name
##
##  echo "MegaUpload 2/2"
##  RCLONE_CONFIG_PASS="[YOUR CONFIG PASSWORD]" rclone --verbose --log-file=/tank/user/logs/$LOGFILE --log-level=DEBUG sync /path/to/archive.tar.lzo.age  megacrypt:bucket
##

echo ""

echo "Edit the below with your config pass, provider names and bucket names"

echo "The purpose of this section is to verify your upload is the complete across providers"

# echo "Clones completed, check for matching file count"

## echo "Backblaze B2"
## RCLONE_CONFIG_PASS="[YOUR CONFIG PASSWORD]" rclone ls blazecrypt:bucket-name
## echo "MegaUpload"
## RCLONE_CONFIG_PASS="[YOUR CONFIG PASSWORD]" rclone ls megacrypt:bucket
